﻿var app = angular.module('bidApp', []);
app.controller('bidControl', function ($scope, $http, $interval) {

    $scope.ResetForm = function () {
        $scope.BidName = "";
        $scope.BidAmount = "";
        $scope.BidRemarks = "";
    }

    $scope.getAllBids = function () {
        $http.get("/BidWebService.asmx/getAllBids")
        .then(function (response) {
            $scope.bids = response.data;
            $scope.predicate = 'x.Timestamp';
            $scope.reverse = false;
            $scope.orderByMe = function (predicate) {
                $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                $scope.predicate = predicate;
            };
        });
    };

    $scope.addBid = function () {
        var item = {
            "BidName": $scope.BidName,
            "BidAmount": $scope.BidAmount,
            "BidRemarks": $scope.BidRemarks
        }

        $http({
            method: 'POST',
            url: '/BidWebService.asmx/addBid',
            data: {
                BidName: $scope.BidName,
                BidAmount: $scope.BidAmount,
                BidRemarks: $scope.BidRemarks
            }
        })
            .then(function successCallback(response) {
                //debugger;
                //$scope.TotalDuration = response.data;
                if (response.data.d > 0) {
                    $scope.alertMessage = "Bid added successfully !!";
                    $scope.ResetForm();
                    $scope.getAllBids();
                }
            }, function errorCallback(response) {
                $scope.alertMessage = "Error! while creating the bid.";
            });
    }


    $interval(function () {
        $scope.getAllBids();
        $scope.alertMessage = "";
    }, 60000);

    $scope.getAllBids();
    $scope.ResetForm();
});