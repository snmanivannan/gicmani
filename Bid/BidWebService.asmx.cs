﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;



namespace Bid
{
    /// <summary>
    /// Summary description for BidWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BidWebService : System.Web.Services.WebService
    {

        BidDataContext dc = new BidDataContext();


        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public void addBid(Object bid)
        public int addBid(string BidName, string BidAmount, string BidRemarks)
        {
            //var x = (Bid)bid;
            int bidAmount;
            if (string.IsNullOrEmpty(BidName) || !Int32.TryParse(BidAmount, out bidAmount)) { return 0; }
            int result = dc.uspInsertBid(BidName, bidAmount, BidRemarks);
            return result;
            //DisplayJSOutput(result);
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void getAllBids()
        {
            var query = from x in dc.uspGetAllBids()
                        select new Bid()
                        {
                            BidName = x.BidName,
                            BidAmount = x.BidAmount,
                            BidRemarks = x.BidRemarks,
                            Timestamp = x.Timestamp
                        };

            //return query.ToList();
            DisplayJSOutput(query.ToList());
            //DisplayJSOutput(dc.uspGetAllBids().ToList());
        }


        public void DisplayJSOutput(object value)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(value));
        }
    }

    public class Bid
    {
        public string BidName { get; set; }
        public int BidAmount { get; set; }
        public string BidRemarks { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
